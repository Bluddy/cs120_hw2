#!/usr/bin/env python

import random as r
import argparse

r.seed()

def gen_seq(file, num):
    with open(file, "w") as f:
        for _ in range(0, num):
            f.write(r.choice(["T", "C", "A", "G", " ", "\n"]))

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("file", type=str,
            help="destination file")
    parser.add_argument("--size", "-s", type=int,
            help="size of file", default=15000)
    args = parser.parse_args()
    gen_seq(file=args.file, num=args.size)

if __name__ == '__main__':
    main()


