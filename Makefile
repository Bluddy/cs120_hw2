# Lines starting with # are comments

# Some variable definitions to save typing later on
CC = gcc
CFLAGS = -std=c99 -Wall -Wextra -pedantic -Wconversion -g

# Default (first) target -- fill this in
hw2:

# Links the test_dnasearch executable
test_dnasearch: test_dnasearch.o dnasearch.o
	$(CC) -o test_dnasearch test_dnasearch.o dnasearch.o

# Compiles dnasearch.c into an object file
dnasearch.o: dnasearch.c dnasearch.h
	$(CC) $(CFLAGS) -c dnasearch.c

# Compiles test_dnasearch.c into an object file
test_dnasearch.o: test_dnasearch.c dnasearch.h
	$(CC) $(CFLAGS) -c test_dnasearch.c

# 'make clean' will remove intermediate, executable files
clean:
	rm -f *.o test_dnasearch hw2
